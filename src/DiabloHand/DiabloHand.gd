extends KinematicBody2D

export (int) var SPEED = 0
var dir = Vector2(1, 0)
var is_moving = true
var is_player_ready = false
const SPEEDUP_RATIO = 2
var is_speedup = false
var speedup_timer
var SPEEDUP_TIME = 0.5

const MOVE_HEIGHT = 50
var move_offset = 0
var move_timer = 0
const MOVE_SPEED_RATIO = 2
var move_step = MOVE_SPEED_RATIO
var to_player = 0

func _ready():
	$Camera2D.global_position = global_position
	pass

func _process(delta):
	$Camera2D.global_position.x = global_position.x
	$Camera2D.global_position.x += $Camera2D.get_viewport_rect().size.x/2.2

	to_player = get_node("/root/global").player_pos.y - ($Sprite.global_position.y + $Sprite.texture.get_height()/2)
	$Sprite.global_position.y += 2 *  delta * MOVE_HEIGHT * sin(move_timer)
	$Sprite.global_position.y += delta * to_player
	
	
	move_timer += delta * move_step 
	
	if move_timer > 1:
		move_step = -MOVE_SPEED_RATIO	
	if move_timer < -1:
		move_step = MOVE_SPEED_RATIO	
	pass
	
func _physics_process(delta):
	var speed = SPEED
	
	# Speed up
	if is_speedup:
		speed *= SPEEDUP_RATIO
		if speedup_timer >= SPEEDUP_TIME:
			is_speedup = false
		speedup_timer += delta
	
	if is_moving and is_player_ready:
		move_and_slide(dir * speed)
	
	if get_slide_count() > 0 :
		if get_node("/root/global").hardcore_mode :
			get_tree().change_scene("res://Levels/Menu/Menu.tscn")
		else :
			get_tree().change_scene(get_tree().current_scene.filename)
	pass

func _on_Player_on_hurt():
	is_speedup = true
	speedup_timer = 0
	pass 


func _on_Level0_on_level_ready():
	is_moving = true


func _on_Player_on_move():
	is_player_ready = true
