extends KinematicBody2D

# Movement
var dir = Vector2()
const GRAVITY = Vector2(0, 256)
const SPEED = 512
var is_moving = true
signal on_move

# Jump
var is_jumping = false
var jump_timer = 0
const JUMP_TIME = 0.4
const JUMP_SPEED = 800

# Hurt
var is_hurted = false
var hurt_timer = 0
const HURT_TIME = 0.1
signal on_hurt

func _ready():
	pass

func _process(delta):
	if is_moving:
		process_input(delta)
		process_anim(delta)
		process_jump(delta)
		process_hurt(delta)
		$Camera2D.global_position = global_position

func process_input(delta):
	dir = Vector2()
	if Input.is_action_pressed("ui_left"):
		dir.x -= 1
	if Input.is_action_pressed("ui_right"):
		dir.x = 1
	if can_jump() and input_action_jump():
		jump()

func process_anim(delta):
	if abs(dir.x) > 0 and not $AnimationPlayer.is_playing():
		$AnimationPlayer.play("Walk")
	if dir.x == 0 :
		$AnimationPlayer.play("Stand")
	
	if dir.x > 0:
		$Sprite.flip_h = false
	if dir.x < 0:
		$Sprite.flip_h = true

func process_jump(delta):
	if is_jumping:
		if jump_timer >= JUMP_TIME:
			is_jumping = false
			
		jump_timer += delta

func process_hurt(delta):
	if is_hurted:
		$Sprite.modulate = Color(1, 0, 0, 1)
		if hurt_timer >= HURT_TIME:
			is_hurted = false
		hurt_timer += delta
	else:
		$Sprite.modulate = Color(1, 1, 1, 1)	
	
func _physics_process(delta):
	var velocity = Vector2()
	
	if is_jumping:
		velocity += Vector2(0, -JUMP_SPEED * sin(1 - (jump_timer/JUMP_TIME)))
	else:
		velocity += GRAVITY
	
	velocity += dir * SPEED
	move_and_slide(velocity, Vector2(0, -1))
	
	get_node("/root/global").player_pos = global_position
	if dir.length() > 0:
		emit_signal("on_move")
		
	# Check collisions
	for col_id in get_slide_count():
		var col = get_slide_collision(col_id)
		var collider = col.collider
		if collider.is_in_group("traps"):
			hurt()

func input_action_jump():
	return (Input.is_action_pressed("ui_accept") or Input.is_action_pressed("ui_up"))
func can_jump():
	return not is_jumping and is_on_floor()
	
func jump():
	is_jumping = true
	jump_timer = 0
	
func hurt():
	$DegatAudio.play()
	emit_signal("on_hurt")
	is_hurted = true
	hurt_timer = 0
	jump()
	

func _on_Level0_on_level_ready():
	is_moving = true
