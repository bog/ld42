extends Node

export (String) var next_level = ""

var title = true
var title_timer = 0
var cube_ame = false
const TITLE_TIME = 3.0

signal on_level_ready

func _ready():
	$Title.percent_visible = 0
	$Title.rect_global_position = Vector2($DiabloHand/Sprite.global_position.x, 0)
	$Player.is_moving = false
	$DiabloHand.is_moving = false

func _process(delta):
	if title:
		if title_timer > TITLE_TIME:
			title = false
			emit_signal("on_level_ready")
		title_timer += delta
		$Title.percent_visible = title_timer/TITLE_TIME

func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		if cube_ame:
			get_tree().change_scene("res://Levels/" + next_level)
		else:
			get_tree().change_scene(get_tree().current_scene.filename)


func _on_Cube_ame_body_entered(body):
	if body.is_in_group("player"):
		cube_ame = true
		$Cube_ame.visible = false
		$Cube_ame/Audio.play()
	 # replace with function body
