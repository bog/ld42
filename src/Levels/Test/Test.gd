extends Node

export (String) var next_level = ""

func _ready():
	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		get_tree().change_scene("res://Levels/" + next_level)

